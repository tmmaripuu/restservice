package local.tm.restservice;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.RestController;


@RestController
@EnableAutoConfiguration
public class MyController {

@Autowired
//    private Model model;


@RequestMapping(value = "/hello", method =RequestMethod.GET)
@ResponseBody
    public String helloResponse() {
    return "Hello world!";
    }
}
